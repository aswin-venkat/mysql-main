var mysql      = require('mysql');
var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://vehicletracker360.firebaseio.com"
  });

  var current_date = new Date();
  var date = current_date.toLocaleString();

var db = admin.firestore();
var userRef = db.collection('personal_users');
var connection = mysql.createConnection({ host: "35.200.241.216",
                                          user: "root",
                                          password: "vtozone",
                                          database: "vt_primary"
                                        });

connection.connect();                                            
 
connection.query('SELECT * FROM user_details', function (error, results) {

  if (error) throw error;
  for(let i=0; i<results.length; i++){

    if(results[i].category == 'pu') {
      
      userRef.doc(results[i].phone).set({
        username: results[i].username,
        phone: results[i].phone,
        email: results[i].email,
        device_id: results[i].device_id,
        message: results[i].message,
        status: results[i].status,
        timestamp: date
      });
    }
  }
    
});

setTimeout(()  => {
  connection.end();
  process.exit();
}, 3000);